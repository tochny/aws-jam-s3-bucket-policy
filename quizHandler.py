import sys
import json
import logging
import boto3
import requests
from botocore.exceptions import ClientError

def upload_file(file_name, bucketName, object_name=None, meta = None):
    """Upload a file to an S3 bucket

    :param file_name: File to upload
    :param bucketName: Bucket to upload to
    :param object_name: S3 object name. If not specified then file_name is used
    :param meta
    :return: True if file was uploaded, else False
    """

    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = file_name

    # Upload the file
    s3 = boto3.resource('s3')
    bucket = s3.Bucket(bucketName)
    try:
        bucket.upload_file(file_name, object_name , 
            ExtraArgs={'ContentType': meta})
    except ClientError as e:
        logging.error(e)
        return False
    return True

def main(args):
    server_ip = str(requests.get('https://api.ipify.org').text)
    sendData = {
        "quiz_slug" : "s3_bucket_policy",
        "callback_url" : "http://"+server_ip+":8887/check"
    }
    data = (requests.post("https://pizg9b4aea.execute-api.us-east-1.amazonaws.com/default/new_token",json=sendData))
    token = json.loads(data.text)["token"]

    client = boto3.client("s3")
    s3 = boto3.resource('s3')
    bucket_name = args[1]
    key = "index.html"
    bucket = s3.Bucket(bucket_name)
    # location = "us-east-1"
    response = client.get_bucket_location(Bucket=bucket_name)
    if(response["LocationConstraint"] is None):
        location = "us-east-1"
    else:
	    location = response["LocationConstraint"]
    url = "https://%s.s3.%s.amazonaws.com/%s" % (bucket_name, location, key)

    with open("/usr/local/bucket.log", "x") as data_file:
        data_file.write(url)
    with open("./docs/index/index.html", "r") as html_file:
        newText = html_file.read().replace("$(tokenID)", token)
    with open("./docs/index/index.html", "w") as html_file:
        html_file.write(newText)

    bucket = args[1]
    upload_file("./docs/index/index.html", bucket, "index.html" , "text/html")
    upload_file("./docs/index/style.css", bucket, "style.css", "text/css")


if __name__ == '__main__':
    main(sys.argv)
