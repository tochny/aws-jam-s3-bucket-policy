import flask
import requests

app = flask.Flask(__name__)
app.config["DEBUG"] = False

@app.route('/check', methods=['GET'])
def check():
    with open("/usr/local/bucket.log", "r") as token_file:
        url = token_file.read()
    test = requests.get(url)
    if(test.status_code == 403):
        data = {
            "success" : True,
            "check_url" : url
        }
        return data
    else:
        data = {
                "success" : False,
                "check_url" : url
            }
        return data
app.run(host='0.0.0.0', port=8887)